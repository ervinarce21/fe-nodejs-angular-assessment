# FE-NodeJS-Angular-Assessment
* This assessment exam is divided into 2 parts, the codes for Node.js (described in section "Rest API) and Angular (described in section "Angular App Search"). You have maximum of 4hrs (2hrs per section) to comply on what is needed for this assessment.  
---
## Prerequisites
* This assessment exam will require the installation of NPM, Node (recommended: 12.19.0), and Angular  
## Process on submission 
1. Make sure to login or signup in Bitbucket
2. Fork this repo as a public repo.
    - ![Fork 1](instruction-images/1_create.png)
    - ![Fork 2](instruction-images/2_Select_to_fork.png)
    - ![Fork 3](instruction-images/3_fork_repository.png)
3. Pull your repo locally.
4. Branch into a feature branch.
5. Develop according to the Acceptance Criteria below on the feature branch. There is no time limit, but this task should not take long.
6. Googling is allowed.
7. When ready to submit, push the feature branch to your public repo.
8. Open a PR from the feature branch to the main branch.
    - ![Create PR 1](instruction-images/4_goto_branch.png)
    - ![Create PR 2](instruction-images/5_initiate_pr.png)
    - ![Create PR 3](instruction-images/6_fillup_pr_details.png)
9. Share the link of your public repo with us.
10. Please contact us if you have any issues or difficulty with this code base.
11. Examine the project architecture.  The project is a functioning server, but it is in no way an optimized one.  Consider what inefficiencies or issues may exist in the project.  You may be asked technical questions about this project in further interviews. Feel free to add any additional features or optimizations beyond the requirements if you wish, but you are not required to do so.  

---
# Exam Details
---
## Section 1: Rest API

* Recommended node version: 12.19.0
* Allotted time: 2hrs
* Command to start the server: `npm start`

### Info
* The Rest API server is using node express framework. It reads an XML document and converts it to a common javascript object.  
* The Rest API server has 3 Endpoints: "/",  "/search/:searchTerm" and "/tags"  

### Acceptance Criteria 

* Map data product record (please use the code below as reference).  
* The product attributes are embedded on the `ProductName` property wrapped in `<span>` tag and separated by a pipe character. It should be added to the product record as an array using the `attributes` property name.  
* The `/` endpoint returns all the processed data from the xml file.
* The exam needs to make the `/search/:searchTerm` work.
    * Search is case insensitive
    * Use `ProductName` property as to compared against the `:searchTerm`
* Collect all unique tags from the `CustomField2` property and return it as list from `/tags` endpoint.


Map data from:  

    {
        ProductCode: "5021416",  
        StockStatus: "1",  
        HideProduct: { },  
        IsChildOfProductCode: "Silver-Tinsel-Tree",  
        Photos_Cloned_From: "Silver-Tinsel-Tree",  
        ProductName: "Silver Stardust Tinsel Tree&reg; <span>|4'|Full 32"|Clear </span>",  
        ProductPopularity: "1195",  
        ProductDescriptionShort: "<span class="size1 clear">clear</span><span class="size2">4'</span><span class="lights badge-"></span>",  
        CustomField2: "R10 H040 W032 COLORTR ALLTREES SLIVER SPARK FULL CLASS CLEAR CHILD",  
        CustomField4: "c80c c48c c90c c70c c102c c9997c",  
        Price_SubText: "<span class="tag sale"></span>",  
        ProductPrice: "149.0000",  
        SalePrice: "129.9900"  
    },  


To:  

    {
        productCode: "5021416",  
        productName: "Silver Stardust Tinsel Tree&reg;",  
        attributes: [ "4'", "Full 32"", "Clear" ],  
        tags: "R10 H040 W032 COLORTR ALLTREES SLIVER SPARK FULL CLASS CLEAR CHILD",  
        productPrice: "149.0000",  
        salePrice: "129.9900",  
        photoClonedFrom: "Silver-Tinsel-Tree"  
    },

---

## Section 2: Angular App Search


* Recommended node version: 12.19.0
* Allotted time: 2hrs
* Command to start the server: `ng serve`

### Info
* The search app is implemented using Angular. 
* The Angular app uses the Rest API from the previous exam. 
* An image of the working app is included and Template for UI is provided
* The app uses the `/search/:searchTerm` endpoint.

### Acceptance Criteria 
The following are needed to make the search function work:
* API call should only start if:
    * 500ms from last keypress
    * Input text is not the same as last
* Search result should show below the input box
* While typing on the search field, result should be show on the left side. Then upon clicking on any of the results, additional info about it will be displayed on the right side. (see search-ui.gif for reference)
  
- ![Sample](exam/search-ui.gif)