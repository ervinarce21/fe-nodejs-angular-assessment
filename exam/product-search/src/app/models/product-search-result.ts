import { Product } from './product';
export interface ProductSearchResult {
  products: Product[];
}
