import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductSearchResult } from '../models/product-search-result';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private url =
    'http://localhost:8200';

  constructor(private http: HttpClient) {}

  search(term: string): Observable<ProductSearchResult> {
    const params = '/search/' + term;

    return this.http
      .get<ProductSearchResult>(this.url +  params);
  }
}
