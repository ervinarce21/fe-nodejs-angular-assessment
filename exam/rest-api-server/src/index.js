import fs from 'fs';
import express from 'express';
import cors from 'cors';
import xmlJsPkg from 'xml2json';
const { toJson } = xmlJsPkg;

import 'dotenv/config.js';

const app = express();
app.use(cors());

console.log('Hello World!');
console.log(process.env.MY_SECRET);

const readFile = function(filename) {
    let _fileContent = '';
    _fileContent = fs.readFileSync(filename, {
        encoding: ''
    });
    return _fileContent;
};

let productsData = JSON.parse(toJson(readFile('./data/product-data.xml')));
if (productsData) {
    productsData = productsData.xmldata;
}

function productDataMap(data) {
    return data;
}

function filterProducts(searchTerm, products) {
    return [];
}

function getTags(products) {
    let uniqueTags = [];
    return uniqueTags;
}

app.get('/', (req, res) => {
    const returnObj = productsData;
    return res.send(returnObj);
});

app.get('/tags', (req, res) => {
    const tags = {
        tags: getTags(productsData.Products)
    }
    return res.send(tags);
});

app.get('/search/:searchTerm', (req, res) => {
    console.log(req);
    let products = filterProducts(req.params.searchTerm, productsData.Products);
    console.log('Search term:', req.params);
    console.log(products);
    let results = {
        ...productsData,
        ...products
    };
    return res.send(results);
});


app.listen(process.env.PORT, () =>
    console.log('App listening on port', process.env.PORT)
);